package lab2.dc.unifil.br;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    
    /**
     * Método main
     * 
     * Método onde o programa será executado 
     * 
     */
    public static void main(String[] args) {
        
        // Realiza benchmarkings
        List<Integer> cobaia = Arrays.asList(7,3,9,2,5,8,1,2,8,5,32,8,9,34,7,3,2);
        
        List<Medicao> medicoes = Cronometro.benchmarkCrescimentoAlgoritmo(
            10, 100, 5, 1,
            () -> new ArrayList<>(cobaia),
            Classificadores::bubblesort
            );

            // Plotta gráfico com resultados levantados
            TabelaTempos tt = new TabelaTempos();
            tt.setTitulo("Tempo para ordenação");
            tt.setEtiquetaX("Qtde elementos lista");
            tt.setEtiquetaY("Tempo (s)");
            tt.setLegendas("Bubblesort", "Insertionsort", "Selectionsort");

            for (int i = 0; i < medicoes.size(); i++) {
                Medicao amostra = medicoes.get(i);
                tt.anotarAmostra(amostra.getN(),
                amostra.getTempoSegundos());    
            }
            tt.exibirGraficoXY();
            
        }
    }
