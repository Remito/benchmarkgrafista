package lab2.dc.unifil.br;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * A classe "Cronometro" pega o tempo total em que o algoritmo foi executado
 * 
 * @author Ricardo Inacio Alvares e Silva
 * @version 20200519
 */
public class Cronometro {

    /**
     * Faz benchmarkings do algoritmo que recebe uma entrada N, inicialmente igual a nInicial,
     * até nFinal, incrementando a nPasso. Retorna uma lista com todas essas medições, classificada
     * do menor N ao maior.
     *
     * @param nInicial Valor escalar de N inicialmente.
     * @param nFinal Valor escalar de N que interrompe as medições.
     * @param nPasso Quantidade de incremento em N a cada iteração de medições.
     * @param repeticoes Quantidade de vezes que cada medição é feita. Quanto maior o valor,
     *                   mais precisa a medição, mas mais demorado o processo total.
     * @param recriadorCobaia Método que recria a estrutura de dados aceita como entrada pelo
     *                        algoritmo em teste, a fim de evitar testes em estruturas já modificadas
     *                        por algoritmos que causam mutação no objeto.
     * @param algoritmo Algoritmo a ser testado.
     * @param <T> Tipo da estrutura de dados do algoritmo.
     * @return Lista com medições de tempo, classificada do menor ao maior N.
     */

    public static <T> List<Medicao> benchmarkCrescimentoAlgoritmo(
            int nInicial, int nFinal, int nPasso, int repeticoes,
            Supplier<T> recriadorCobaia,
            Consumer<T> algoritmo){

        List<Medicao> valores = new ArrayList<>();
        
        for (int i = nInicial; i < nFinal; i += nPasso){
            T cobaia = recriadorCobaia.get();
            double timeStart = (System.nanoTime());

            algoritmo.accept(cobaia);

            double timeStop = (System.nanoTime());
            double totalTime = (timeStop - timeStart) * 1e-9;
    
            Medicao medicao = new Medicao(i, totalTime);
            valores.add(medicao);
        }

        return valores;
    }

    /**
     * O método procura qual é o menor tempo realizado pelo algoritmo e retorna esse valor.
     * 
     * @param <T> Estrutura de dados.
     * @param recriadorCobaia Método que recria a estrutura de dados aceita como entrada pelo
     *                        algoritmo em teste, a fim de evitar testes em estruturas já 
     *                        modificadas por algoritmos que causam mutação no objeto.
     * @param algoritmo Algoritmo a ser testado.
     * @param repeticoes Quantidade de vezes que cada medição é feita.
     * @return Retorna o menor tempo realizado pelo algoritmo.
     */
    public static <T> double benchmarkAlgoritmo(Supplier<T> recriadorCobaia, Consumer<T> algoritmo, int repeticoes) {
        Cronometro cron = new Cronometro();
        double menorTempo = Double.POSITIVE_INFINITY;
        for (int i = 0; i < repeticoes; i++) {
            T cobaia = recriadorCobaia.get();

            cron.zerar();
            cron.iniciar();
            algoritmo.accept(cobaia);
            double ultimoTempo = cron.parar();

            menorTempo = Math.min(menorTempo, ultimoTempo);
        }

        return menorTempo;
    }           

    /**
     * Construtor padrão da classe.
     */
    public Cronometro() {
        //throw new RuntimeException("O aluno ainda não implementou essa funcionalidade.");
    }
    
    /**
     * Inicia ou reinicia a contagem de tempo. Nunca zera o último estado do contador. Se o tempo já
     * estiver correndo, não faz nada.
     */
    public void iniciar() {
        if(comecou == false){
            startTime = 0;
            startTime = System.nanoTime();
            comecou = true;
        }
    }
    
    /**
     * Para a contagem de tempo e retorna uma leitura do tempo decorrido.
     * 
     * @return Tempo decorrido até o momento da parada, em milissegundos.
     */
    public double parar() {
        comecou = false;
        stopTime = System.nanoTime();
        double totalTime = stopTime - startTime;
        return totalTime;
        //        throw new RuntimeException("O aluno ainda não implementou essa funcionalidade.");
    }
    
    /**
     * Retorna o tempo decorrido contado até então, independente se está parado ou correndo. Não
     * altera o estado de contagem (parado/correndo).
     * 
     * @return Tempo decorrido contado pelo cronômetro, em milissegundos.
     */
    public double lerTempoEmMilissegundos() {
        double totalMiliTime = (stopTime - startTime) * 1000;
        return totalMiliTime;
    }
    
    /**
     * Zera o contador de tempo do cronômetro. Se o cronômetro estava em estado de contagem, ele é
     * parado.
     */
    public void zerar() {
        parar();
        startTime = 0;
    }
    
    // Atributos da classe são declarados aqui
    private long startTime;
    private long stopTime;
    private boolean comecou;
     
}