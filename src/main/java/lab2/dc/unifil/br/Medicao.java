package lab2.dc.unifil.br;

public class Medicao {

    /**
     * Construtor da classe
     */
    public Medicao(int n, double tempoSegundos) {
        this.n = n;
        this.tempoSegundos = tempoSegundos;
    }

    /**
     * @return retorna o valor(em int) "encontrado"
     */
    public int getN(){
        return n;
    }

    /**
     * @return retorna o tempo(em double) "encontrado"
     */
    public double getTempoSegundos(){
        return tempoSegundos;
    }

    /**
     * Atributos da classe
     */
    private int n;
    private double tempoSegundos;
}
