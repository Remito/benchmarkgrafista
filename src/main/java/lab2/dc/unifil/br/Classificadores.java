package lab2.dc.unifil.br;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Classificadores {

    public enum metodoDeOrdenacao{
        CRESCENTE, DECRESCENTE;
    }
    
    //Bogosort foi removido por escolha do professor

    /**
     * Ele permuta dois elementos (a e b) da lista
     * θ(1)
     * @param lista É a slista a ser organizada
     * @param a número a ser comparado com o b
     * @param b número a ser comparado com o a
     */
    private static void permutar(List<Integer> lista, int a, int b) {
        Integer permutador = lista.get(a); //1
        lista.set(a, lista.get(b)); //1
        lista.set(b, permutador); //1
    }
   
    /**
     * Método de organização de uma lista por Bubblesort
     * O(n²)
     * Ω(n)
     * 
     * @param lista lista a ser organizada de forma crescente
     */
    //bubblesort    
    public static void bubblesort(List<Integer> lista) {
        boolean houvePermuta;
        do {
            houvePermuta = false; //1
            // Sobe a bolha
            for (int i = 1; i < lista.size(); i++) { //n

                if (lista.get(i-1) > lista.get(i)) { //n  //n*n
                    permutar(lista, i - 1, i); //n
                    houvePermuta = true; //1
                }
            }
        } while (houvePermuta); //n
    }
    
    /**
     * Método de organização de uma lista por Bubblesort
     * O(n²)
     * Ω(n)
     * @param lista lista a ser organizada de forma decrescente
     */
    //bubblesort decrescente temporário (até achar uma solução melhor)
    public static void bubblesortDecrescente(List<Integer> lista) {
        boolean houvePermuta;
        do {
            houvePermuta = false;
            // Sobe a bolha
            for (int i = 1; i < lista.size(); i++) {
                if (lista.get(i-1) < lista.get(i)) {
                    permutar(lista, i - 1, i);
                    houvePermuta = true;
                }
            }
        } while (houvePermuta);
        System.out.println(lista);
    }

    /**
     * O método recebe uma lista a ser ordenada e dois índices 
     * (um o começo o outro o final para ser ordenado)
     * O(n²)
     * Ω(n) 
     * @param vals lista a ser ordenada
     * @param e variável que representa o índice inicial.
     * @param d variável que representa o índice final.
     */
    //insertionsort
    public static void insertionsort(List<Integer> vals, int e, int d){
        for (int i = e; i <= d; i++) { //n
            Integer elem = vals.get(i); //1

            int j = i; //1
                while (j > 0 && vals.get(j-1) > elem) { //n  //n*n
                    vals.set(j, vals.get(j-1)); //n

                j--; //1
            }
            vals.set(j, elem); //n
        }
    }


    /**
     * O método recebe uma lista a ser ordenada e dois índices 
     * (um o começo o outro o final para ser ordenado)
     * O(n²)
     * Ω(n) 
     * @param vals lista a ser ordenada de forma decrescente.
     * @param e variável que representa o índice inicial.
     * @param d variável que representa o índice final.
     */
    //insertionsort decrescente temporário (até achar uma solução melhor)
    public static void insertionsortDecrescente(List<Integer> vals, int e, int d){
        for (int i = e; i <= d; i++) { //n
            Integer elem = vals.get(i); //1

            int j = i; //1
                while (j > 0 && vals.get(j-1) < elem) { //n  //n*n
                    vals.set(j, vals.get(j-1));//n

                j--; //1
            }

            vals.set(j, elem); //n
        }
    }

    /**
     * O método ordena a lista por meio de mergesort (se lista.size() > k) caso contrário ele 
     * vai ser ordenado por meio de insertionsort
     * θ(n log(n))
     * @param lista lista a ser ordenada de forma crescente
     * @param k um k tamanho que vai determinar se será feito o mersort ou o insertionsort.
     */
    public static void mergesort(List<Integer> lista, int k) {
        if(lista.size() > k){ //n
            // Casos base
            if (lista.size() <= 1) return; //1
            // Casos de subdivisão recursiva
            final int idxMeioLista = lista.size() / 2; //1
            List<Integer> esquerda = new ArrayList<Integer>(lista.subList(0, idxMeioLista)); //1
            List<Integer> direita = new ArrayList<Integer>(lista.subList(idxMeioLista, lista.size()));//1

            mergesort(esquerda, k); //

            mergesort(direita, k); //

            merge(lista, esquerda, direita); //

        }else{
            int e = 0;//1
            int d = k;//1
            insertionsort(lista, e, d);//
        }
    }

    /**
     * O método junta as listas esquerda e direita para formar uma única lista (repete o processo
     * até a lista inteira for ordenada)
     * θ(n)
     * @param lista lista a ser ordenada de forma crescernte
     * @param esquerda lista do lado esquerdo separada da lista original (lista)
     * @param direita lista do lado direito separada da lista original (lista)
     */
    private static void merge(List<Integer> lista, List<Integer> esquerda, List<Integer> direita) {
        int idxE = 0, idxD = 0, idxL = 0; //1
        while (idxE < esquerda.size() && idxD < direita.size()) {//n
            if (esquerda.get(idxE) < direita.get(idxD)) {//n
                lista.set(idxL, esquerda.get(idxE));//1
                
                idxE++;//1
            } else {
                lista.set(idxL, direita.get(idxD));//1
                
                idxD++;//1
            }
            idxL++;//1
        }

        int idxF;
        List<Integer> faltantes;
        if (idxE < esquerda.size()) {//n
            
            faltantes = esquerda;//1
            idxF = idxE;//1
        } else {
            faltantes = direita;//1
            idxF = idxD;//1
        }

        while (idxF < faltantes.size()) {//n
            lista.set(idxL, faltantes.get(idxF));//1
            idxL++; idxF++; //1
        }
    }

    /**
     * O método ordena a lista por meio de mergesort (se lista.size() > k) caso contrário ele 
     * vai ser ordenado por meio de insertionsort
     * θ(n log(n)) 
     * @param lista lista a ser ordenada de forma decrescente
     * @param k um k tamanho que vai determinar se será feito o mersort ou o insertionsort.
     */
    //mergesort decrescente temporário (até achar uma solução melhor)
    public static void mergesortDecrescente(List<Integer> lista, int k) {
        if(lista.size() > k){
            // Casos base
            if (lista.size() <= 1) return;
            // Casos de subdivisão recursiva
            final int idxMeioLista = lista.size() / 2;
            List<Integer> esquerda = new ArrayList<Integer>(lista.subList(0, idxMeioLista));
            List<Integer> direita = new ArrayList<Integer>(lista.subList(idxMeioLista, lista.size()));

            mergesortDecrescente(esquerda, k);

            mergesortDecrescente(direita, k);

            mergeDecerscente(lista, esquerda, direita);
            System.out.println(lista);
        }else{
            int e = 0;
            int d = k;
            insertionsortDecrescente(lista, e, d);
        }
    }

    /**
     * O método junta as listas esquerda e direita para formar uma única lista (repete o processo
     * até a lista inteira for ordenada)
     * θ(n) 
     * @param lista lista a ser ordenada de forma decrescernte
     * @param esquerda lista do lado esquerdo separada da lista original (lista)
     * @param direita lista do lado direito separada da lista original (lista)
     */
    private static void mergeDecerscente(List<Integer> lista, List<Integer> esquerda, List<Integer> direita) {
        int idxE = 0, idxD = 0, idxL = 0;
        while (idxE < esquerda.size() && idxD < direita.size()) {
            System.out.println(lista);
            
            if (esquerda.get(idxE) > direita.get(idxD)) {
                lista.set(idxL, esquerda.get(idxE));
                
                idxE++;
            } else {
                lista.set(idxL, direita.get(idxD));
                
                idxD++;
            }
            idxL++;
        }

        int idxF;
        List<Integer> faltantes;
        if (idxE < esquerda.size()) {
            
            faltantes = esquerda;
            idxF = idxE;
        } else {
            faltantes = direita;
            idxF = idxD;
        }

        while (idxF < faltantes.size()) {
            lista.set(idxL, faltantes.get(idxF));
            idxL++; idxF++;
        }
    }

    /**
     * O método ordena a lista por meio de selectionsort
     * θ(n)
     * @param lista lista a ser ordenada
     */
    public static void selectionsort(List<Integer> lista) {
        for (int i = 0; i < lista.size(); i++) { //n
            int menorIdx = encontrarIndiceMenorElem(lista, i);//n
            permutar(lista, menorIdx, i);//n
        }
    }

    /**
     * O método encontra o menor elemento da lista e retorna esse valor
     * O(n²)
     * Ω(n) 
     * @param lista lista a ser ordenada
     * @param idxInicio o índex que ele vai começar a procurar
     * @return retorna o menor elemento
     */
    private static int encontrarIndiceMenorElem(List<Integer> lista, int idxInicio) {
        int menor = idxInicio;//1
        for (int i = idxInicio+1; i < lista.size(); i++) { //n
            if (lista.get(menor) > lista.get(i)) menor = i; //n  //n*n
        }
        return menor; //1
    }

    /**
     * O método ordena a lista por meio de selectionsort
     * θ(n)
     * @param lista lista a ser ordenada
     */
    //selectionsort decrescente temporário (até achar uma solução melhor)
    public static void selectionsortDecrescente(List<Integer> lista) {
        for (int i = 0; i < lista.size(); i++) {
            int maiorIdx = encontrarIndiceMaiorElem(lista, i);
            System.out.println(lista);
            permutar(lista, maiorIdx, i);
        }
    }

    /**
     * O método encontra o maior elemento da lista e retorna esse valor
     * O(n²)
     * Ω(n) 
     * @param lista lista a ser ordenada
     * @param idxInicio o índex que ele vai começar a procurar
     * @return retorna o menor elemento
     */
    private static int encontrarIndiceMaiorElem(List<Integer> lista, int idxInicio) {
        int maior = idxInicio;
        for (int i = idxInicio+1; i < lista.size(); i++) {
            if (lista.get(maior) < lista.get(i)) maior = i;
        }
        return maior;
    }

    /**
     * O método ordena a lista por meio de quicksort
     * θ(log(n))
     * @param lista lista a ser ordenada
     */
    public static void quicksort(List<Integer> lista) {
        quick(lista, 0, lista.size() - 1); //
    }

    /**
     * O método compara o pivo com os outros elementos da lista (apartir do último elemento) até ele 
     * chegar a ele mesmo ou encontrar um elemento menor que ele (caso isso acontecer, os elementos 
     * vão trocar de posição), isso se repetirá até que a lista esteje ordenada.
     * O(n²)
     * Ω(n log(n))
     * @param lista lista a ser ordenada
     * @param inicio primeiro índice da lista
     * @param fim   último índice da lista
     */
    public static void quick(List<Integer> lista, int inicio, int fim) {
        if (inicio < fim) { //n
            int pivo = inicio; //1
            int esquerda = inicio + 1; //1
            int direita = fim;  //1
            int pivoValor = lista.get(pivo); //1
            while (esquerda <= direita) { //n
                while (esquerda <= fim && pivoValor >= lista.get(esquerda)) { //n
                    esquerda++; //1
                }
                while (direita > inicio && pivoValor < lista.get(direita)) { //n
                    direita--; //1
                }
                if (esquerda < direita) { //n
                    Collections.swap(lista, esquerda, direita); //n
                }
            }
            Collections.swap(lista, pivo, esquerda - 1); //n
            quick(lista, inicio, direita - 1);  //
            quick(lista, direita + 1, fim);  //
        }
    }

    /**
     * O método ordena a lista por meio de quicksort
     * θ(log(n))
     * @param lista lista a ser ordenada
     */
    //quicksort decrescente temporário (até achar uma solução melhor)
    public static void quicksortDecrescente(List<Integer> lista) {
        quickDecrescente(lista, 0, lista.size() - 1);
    }

    /**
     * O método compara o pivo com os outros elementos da lista (apartir do último elemento) até ele 
     * chegar a ele mesmo ou encontrar um elemento maior que ele (caso isso acontecer, os elementos 
     * vão trocar de posição), isso se repetirá até que a lista esteje ordenada.
     * O(n²)
     * Ω(n log(n))
     * @param lista lista a ser ordenada
     * @param inicio primeiro índice da lista
     * @param fim   último índice da lista
     */
    public static void quickDecrescente(List<Integer> lista, int inicio, int fim) {
        if (inicio < fim) {
            int pivo = inicio;
            int esquerda = inicio + 1;
            int direita = fim;  
            int pivoValor = lista.get(pivo);
            while (esquerda <= direita) {
                System.out.println(lista);
                while (esquerda <= fim && pivoValor <= lista.get(esquerda)) {
                    esquerda++;
                }
                while (direita > inicio && pivoValor > lista.get(direita)) {
                    direita--;
                }
                if (esquerda < direita) {
                    Collections.swap(lista, esquerda, direita);
                }
            }
            Collections.swap(lista, pivo, esquerda - 1);
            quickDecrescente(lista, inicio, direita - 1);
            quickDecrescente(lista, direita + 1, fim);
        }
    }

    /**
     * Atributos da classe
     */
    private static Random rng = new Random("Seed constante repetível".hashCode());
}